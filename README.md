# SVG with AngularJS
	
Displaying  nodes and edges in SVG with AngularJS.

This is one of my experiments with AngularJS creating and manipulating SVG DOM. 
After three days trial and error (including learning AngularJS and most of SVG stuff) I came to think: Yes, AngularJS just loves SVG. 
But I don't want to be hasty, there's more to explore.


## Got some stuff from:

* Some ideas from [Replacing (most of) d3.js with pure SVG + AngularJS](http://alexandros.resin.io/angular-d3-svg/)
* Project scaffold from [Yeoman](http://yeoman.io/)
* Using and still learning [AngularJS](https://angularjs.org/)
* Collection support from [angular-collection](https://github.com/tomkuk/angular-collection)
* Applying pan &amp; zoom on any DOM element with [angular-pan-zoom](https://github.com/mvindahl/angular-pan-zoom) 
* Flat layout for [Bootstrap](http://getbootstrap.com/) from [Bootflat](http://bootflat.github.io/)


## Screenshot

![angular-and-svg.png](https://bitbucket.org/repo/joaEnB/images/1306971015-angular-and-svg.png)


## License

![Creative Commons Attribution 4.0 International License](https://i.creativecommons.org/l/by/4.0/80x15.png)

This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by/4.0/](http://creativecommons.org/licenses/by/4.0/).


## Find
* [me @ Bitbucket](https://bitbucket.org/resah/)
* [me @ GitHub](https://github.com/resah)