'use strict';

/**
 * @ngdoc service
 * @name jsAngularSvgApp.Edge
 * @description
 * # Edge
 * Factory in the jsAngularSvgApp.
 */
angular.module('jsAngularSvgApp').factory('Edge', [ function () {

	var edge = function(values) {
		
		this.id = values.id || 0;
		this.fromNodeId = values.fromNodeId || 0;
		this.toNodeId = values.toNodeId || 0;
		this.type = values.type || 'edge';
		
		this.fromNode = {};
		this.toNode = {};
	};
	
    return (edge);
}]);
