'use strict';

/**
 * @ngdoc service
 * @name jsAngularSvgApp.EdgeCollection
 * @description # EdgeCollection Factory in the jsAngularSvgApp.
 */
angular.module('jsAngularSvgApp').factory('EdgeCollection', ['$collection', function($collection) {
	var EdgeCollection = $collection;
	return (EdgeCollection);
}]);
