'use strict';

/**
 * @ngdoc service
 * @name jsAngularSvgApp.NodeService
 * @description # NodeService Service in the jsAngularSvgApp.
 */
angular.module('jsAngularSvgApp').service('NodeService', [ '$http', 'NodeCollection', 'Node', function($http, NodeCollection, Node) {

	var promise;
	
	var nodeService = {
		async : function() {
			if (!promise) {
				promise = $http({ 
						url: 'scripts/mocks/nodes.json', 
						method: 'GET', 
						cache: true
					}).then(
					function(response) {
						
						var collection = NodeCollection.getInstance({idAttribute: 'id'});
						angular.forEach(response.data, function(nodeValues) {
							var node = new Node(nodeValues);
							collection.add(node);
						});
						return collection;
					});
			}
			return promise;
		}
	};
	
	return nodeService;
}]);
