'use strict';

/**
 * @ngdoc service
 * @name jsAngularSvgApp.EdgeService
 * @description # EdgeService Service in the jsAngularSvgApp.
 */
angular.module('jsAngularSvgApp').service('EdgeService', [ '$http', 'EdgeCollection', 'Edge', function($http, EdgeCollection, Edge) {

	var promise;
	
	var edgeService = {
		async : function() {
			if (!promise) {
				promise = $http({ 
						url: 'scripts/mocks/edges.json', 
						method: 'GET', 
						cache: true
					}).then(
					function(response) {

						var collection = EdgeCollection.getInstance({idAttribute: 'id'});
						angular.forEach(response.data, function(edgeValues) {
							var edge = new Edge(edgeValues);
							collection.add(edge);
						});
						return collection;
					});
			}
			return promise;
		}
	};
	
	return edgeService;
}]);
