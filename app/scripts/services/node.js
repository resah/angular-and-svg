'use strict';

/**
 * @ngdoc service
 * @name jsAngularSvgApp.Node
 * @description
 * # Node
 * Factory in the jsAngularSvgApp.
 */
angular.module('jsAngularSvgApp').factory('Node', [ function () {

	var node = function(values) {
		
		this.id = values.id || 0;
		this.name = values.name || 'node-' + this.id;
		this.x = values.x || 0;
		this.y = values.y || 0;
		this.weight = values.weight || 0;
		
		this.incoming = [];
		this.outgoing = [];

		/**
		 * Get index of outgoing edge.
		 */
		this.outgoingIndexOf = function(edge) {
			return this.outgoing.indexOf(edge.id);
		};
		
		/**
		 * Get index of incoming edge.
		 */
		this.incomingIndexOf = function(edge) {
			return this.incoming.indexOf(edge.id);
		};
	};
	
    return (node);
}]);
