'use strict';

/**
 * @ngdoc service
 * @name jsAngularSvgApp.NodeCollection
 * @description # NodeCollection Factory in the jsAngularSvgApp.
 */
angular.module('jsAngularSvgApp').factory('NodeCollection', ['$collection', function($collection) {
			
	var NodeCollection = $collection;
//	var NodeCollection = $collection.extend({
//		constructor : function(options) {
//			options || (options = {});
//			if (options.comparator !== void 0) {
//				this.comparator = options.comparator;
//			}
//			this.idAttribute = options.idAttribute || this.idAttribute;
//			this.current = null;
//			this._reset();
//			this.initialize.apply(this, arguments);
//		}
//	});
	
	return (NodeCollection);
}]);