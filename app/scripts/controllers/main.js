'use strict';

/**
 * @ngdoc function
 * @name jsAngularSvgApp.controller:MainCtrl
 * @description # MainCtrl Controller of the jsAngularSvgApp
 */
angular.module('jsAngularSvgApp').controller('MainCtrl', ['$scope', 'NodeService', 'EdgeService', 
    function($scope, NodeService, EdgeService) {

	// setup zoom/pan for SVG
	$scope.panzoomConfig = {
		zoomLevels : 8
		// others are initialized with default values
	};
	$scope.panzoomModel = {}; // always pass empty object

	// load data
    NodeService.async().then(function(nodes) {

    	$scope.nodes = nodes;

    	EdgeService.async().then(function(edges) {
    		
    		$scope.edges = edges;
    		
    		angular.forEach($scope.edges.all(), function(edge) {
    			
    			var fromNode = $scope.nodes.get(edge.fromNodeId);
    			var toNode = $scope.nodes.get(edge.toNodeId);

    			fromNode.outgoing.push(edge);
    			toNode.incoming.push(edge);
    			
    			edge.fromNode = fromNode;
    			edge.toNode = toNode;
    			$scope.edges.add(edge);
    		});
    	});
    });

}]);
