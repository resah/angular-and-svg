'use strict';

/**
 * @ngdoc directive
 * @name jsAngularSvgApp.directive:svgConnectorDirective
 * @description # svgConnectorDirective
 */
angular.module('jsAngularSvgApp').directive('svgConnectorDirective', function() {
	return {
		restrict : 'E',
		replace : true,
		scope : {
			edge : '='
		},
		templateUrl : 'templates/svgconnectordirective.html',
		templateNamespace : 'svg'
	};
});
