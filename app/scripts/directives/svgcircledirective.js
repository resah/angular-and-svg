'use strict';

/**
 * @ngdoc directive
 * @name jsAngularSvgApp.directive:svgCircleDirective
 * @description # svgCircleDirective
 */
angular.module('jsAngularSvgApp').directive('svgCircleDirective', function() {
	return {
		restrict : 'E',
		replace : true,
		scope : {
			node : '='
		},
		template : '<circle class="node" data-ng-attr-cx="{{node.x}}" data-ng-attr-cy="{{node.y}}" data-ng-attr-r="{{node.weight}}"/>',
		templateNamespace : 'svg'
	};
});
