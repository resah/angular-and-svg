'use strict';

/**
 * @ngdoc overview
 * @name jsAngularSvgApp
 * @description
 * # jsAngularSvgApp
 *
 * Main module of the application.
 */
angular
  .module('jsAngularSvgApp', [
//    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'panzoom',
    'ngCollection'
  ])
  .config( [ '$routeProvider', function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }]);
